package com.xueliman.iov.auth_server_resource_common.other;

/**
 * @author zxg
 */
public class ApiException extends RuntimeException {
    private IResultCode resultCode;

    public ApiException(IResultCode resultCode) {
        super(resultCode.getMessage());
        this.resultCode = resultCode;
    }

    public ApiException(String message) {
        super(message);
        this.resultCode = ResultCode.FAILURE;
    }

    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public IResultCode getResultCode() {
        return this.resultCode;
    }
}
